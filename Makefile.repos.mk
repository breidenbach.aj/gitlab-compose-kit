data:
	mkdir -p data

gitaly/.git:
	git clone https://gitlab.com/gitlab-org/gitaly.git
	git -C gitaly remote set-url origin --push git@gitlab.com:gitlab-org/gitaly.git

gitlab-shell/.git:
	git clone https://gitlab.com/gitlab-org/gitlab-shell.git
	git -C gitlab-shell remote set-url origin --push git@gitlab.com:gitlab-org/gitlab-shell.git

gitlab-pages/.git:
	git clone https://gitlab.com/gitlab-org/gitlab-pages.git
	git -C gitlab-pages remote set-url origin --push git@gitlab.com:gitlab-org/gitlab-pages.git

gitlab-rails/.git:
	git clone https://github.com/onecommons/gitlab-oc

unfurl/.git:
	git clone https://github.com/onecommons/unfurl

tyk-unfurl-auth-middleware/.git:
	git clone https://gitlab.com/onecommons/tyk-unfurl-auth-middleware

.PHONY: repos
repos: gitaly/.git gitlab-shell/.git gitlab-pages/.git gitlab-rails/.git unfurl/.git tyk-unfurl-auth-middleware/.git data

.PHONY: deps
deps: repos

.PHONY: update-repos
update-repos: repos
	git pull
	git -C gitaly pull
	git -C gitlab-rails pull
	git -C gitlab-shell pull

	git -C unfurl pull
	git -C tyk-unfurl-auth-middleware pull

.PHONY: latest-master
latest-master: repos
	git -C gitaly checkout master
	git -C gitaly pull
	git -C gitlab-rails checkout master-foss
	git -C gitlab-rails pull
	git -C gitlab-rails checkout main
	git -C gitlab-rails pull
	git -C gitlab-shell checkout main
	git -C gitlab-shell pull

	git -C unfurl checkout main
	git -C unfurl pull
	git -C tyk-unfurl-auth-middleware checkout main
	git -C tyk-unfurl-auth-middleware pull

	make down
