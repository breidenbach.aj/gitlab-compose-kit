#!/bin/bash

cd /home/git/gitlab || exit

command="
  deploy_image = Ci::InstanceVariable.find_by(key: 'DEPLOY_IMAGE')
  deploy_image.value = 'ghcr.io/onecommons/unfurl:$UNFURL_BRANCH'
  deploy_image.save!
"

(env | grep -q UNFURL_BRANCH) && echo "$command" | bin/rails c
