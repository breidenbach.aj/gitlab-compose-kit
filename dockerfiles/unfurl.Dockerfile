FROM python:3.11.6-slim

ENV UNFURL_HOME=""

RUN apt-get -qq update && \
    apt-get -qq install -y git curl unzip wget expect entr && \
    apt-get -qq clean -y && rm -rf /var/lib/apt/lists/*


ARG UNFURL_BRANCH=main
RUN pip3 install "unfurl[server] @ git+https://github.com/onecommons/unfurl.git@$UNFURL_BRANCH#egg=unfurl"

ADD /oc-scripts /oc-scripts

WORKDIR /src/unfurl
RUN git config --global --add safe.directory /src/unfurl
#ENTRYPOINT /oc-scripts/unfurl-startup.sh
ENTRYPOINT gunicorn --reload unfurl.server.serve:app
