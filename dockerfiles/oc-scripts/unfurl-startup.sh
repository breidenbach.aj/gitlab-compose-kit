#!/bin/bash

function not_ignored {
  for f in $(find . -type f -a -not -path './.git/*'); do
    if ! $(git check-ignore -q $f 2>/dev/null); then echo $f; fi
  done
}

not_ignored | entr /oc-scripts/restart-gunicorn.sh $@
