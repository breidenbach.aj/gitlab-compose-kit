#!/bin/bash

#there's also reload which we could try without entr

kill $(cat /tmp/gunicorn.pid 2>/dev/null) 2>/dev/null || /bin/true

gunicorn -p /tmp/gunicorn.pid --capture-output --log-file /dev/stdout unfurl.server:app $@ &
